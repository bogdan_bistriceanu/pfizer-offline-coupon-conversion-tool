// begin global variables
// for debugging
var x, d = new Date(),
    random = Math.floor(Math.random() * 100000),
    uniqueBrands = [],
    fileHolder, headers;
// end global variables

var $ = $ || window.jQuery;

function csvTojs(csv) {
    var lines = csv.split("\n"), i,
        result = [], obj = {}, key,
        row, queryIdx, startValueIdx, idx, c, value;

    headers = lines[0].replace(/\r/g,"").split(",");

    for (i = 1; i < lines.length; i = i + 1) {
        
        obj = {};
        row = lines[i];
        queryIdx = 0;
        startValueIdx = 0;
        idx = 0;

        if (row.trim() === '') {
            continue;
        }

        while (idx < row.length) {
            /* if we meet a double quote we skip until the next one */
            c = row[idx];

            if (c === '"') {
                do {
                    c = row[++idx];
                } while (c !== '"' && idx < row.length - 1);
            }

            if (c === ',' || /* handle end of line with no comma */ idx === row.length - 1) {
                /* we've got a value */
                value = row.substr(startValueIdx, idx - startValueIdx).trim();

                /* skip first double quote */
                if (value[0] === '"') {
                    value = value.substr(1);
                }
                /* skip last comma */
                if (value[value.length - 1] === ',') {
                    value = value.substr(0, value.length - 1);
                }
                /* skip last double quote */
                if (value[value.length - 1] === '"') {
                    value = value.substr(0, value.length - 1);
                }

                key = headers[queryIdx++];
                obj[key] = value;
                startValueIdx = idx + 1;
            }

            ++idx;
        }

        result.push(obj);
    }
    return result;
}

// main upload function
function process(file) {
    fileHolder = file;
    $('#placeholder').val(file.name);
    if (file.type.match(/text\/csv/) || file.type.match(/vnd\.ms-excel/)) {
        var oFReader = new FileReader();
        oFReader.onloadend = function () {

            //storing our parsed CSV into our variable by calling the csvTojs function
            x = csvTojs(this.result);
        };
        oFReader.readAsText(file);
        $("span.insideTxt").html("File uploaded.");
    } else {
        $("span.insideTxt").html("This file does not seem to be a CSV.");
    }
}

//we define arrays that will hold ALL of our information
var cid = [];
var store = [];
var brand = [];
var date = [];
var printDate = [];
var program = [];

//we define another set of arrays that will hold only values from rows where ID is present
var $cid = []; //holds the cid values
var $store = []; //holds the store names, lowercase, stripped of periods
var $brand = []; //holds the brand names, lowercase, stripped of spaces
var $date = []; //holds the date of purchase
var $printDate = []; //holds the date the coupon is printed
var $program = []; //holds the program/campaign name, lowercase, stripped of spaces

function processData() {
    $('#devproperty, #prodproperty').removeAttr('disabled');
    var i, z, item;
    try {
        if (fileHolder.name !== undefined) {
            $("span.insideTxt").html("Processing uploaded file ...");
            $("#status, #brands .list-group, .csvHeaders").empty();
            for (item in headers) {
                $('.csvHeaders').append('<label class="label label-default">' + headers[item] + '</label> ');
            }
        }
    } catch (e) {
        $("span.insideTxt").html("<b>Please upload a CSV file</b>");
        return;
    }

    // reset all arrays
    cid = [];
    store = [];
    brand = [];
    date = [];
	printDate = [];
    program = [];
    uniqueBrands = [];

    $cid = [];
    $store = [];
    $brand = [];
    $date = [];
	$printDate = [];
    $program = [];


    //loop through the array and extract the good stuff
    //we need brand, the cid/ga, the date, and the  store name 
    for (i = 0; i < x.length; i = i + 1) {
        cid.push(x[i]['ga']);
        store.push(x[i]['Clean Retailer Name'].toLowerCase().replace(".", ""));
        brand.push(x[i]['Brand (Select Only)'].toLowerCase().replace(" ", ""));
        date.push(x[i]['Purchase Date'].replace(/-/g,''));
        program.push(x[i]['Program Name'].toLowerCase().replace(" ", ""));
    }

    //loop through the arrays again, this time checking for blank ID values
    //will only push values if ID is present and if ID NOT EQUAL to 0
    for (z = 0; z < cid.length; z = z + 1) {
        if (cid[z] !== '""' && cid[z] !== '' && cid[z] !== '"0"' && cid[z] !== '0' && cid[z] !== undefined && cid[z] !== null && cid[z] !== 'undefined') {
            if (cid[z].trim() !== '') {
                $cid.push(cid[z]);
                $store.push(store[z]);

                /* check what brand this of advil this is? */
                if (brand[z] === 'advil') {
                    if (program[z].match(/advilpm|advil pm|pm\.advil\.com|pm advil|pmadvil/)) {
                        brand[z] = 'advilpm';
                    } else if (program[z].match(/advil childrens|advil children\'s|advilchildrens|advilchildren\'s|childrens advil|children\'s advil|childrensadvil|children\'sadvil|infant/)) {
                        brand[z] = 'advilchild';
                    }  else if (program[z].match(/advil respiratory|advilrespiratory|respiratory advil|respiratoryadvil|advil allergy|sinus/)) {
                        brand[z] = 'advilresp';
                    }
                }
                $brand.push(brand[z]);
                $date.push(date[z]);
                $program.push(program[z]);

                // push each item into its respective brand array
                if (typeof window[brand[z]] === 'undefined') {
                    window[brand[z]] = [];
                    uniqueBrands.push(brand[z]);
                }
                window[brand[z]].push([brand[z], cid[z], date[z], store[z]]);
            }
        }

    }

    $("#status").append("<p> Total rows of data: <b>" + cid.length + "</b></p>");
    $("#status").append("<p> Rows with valid GA client ID values <b>" + $cid.length + "</b></p>");
    
    for (i = 0; i < uniqueBrands.length; i = i + 1) {
        
        $("#brands ul").append("<li class='list-group-item'><span class='label label-default label-pill pull-right'>" + window[uniqueBrands[i]].length + "</span>" + uniqueBrands[i].toUpperCase() + "</li>");
        
    }
    

}


function sendHit() {

    $('#devproperty, #prodproperty').attr('disabled','disabled');

    var start = Date.now(),
    totalHits = $cid.length,
    groupings = 40,
    count = 0;

    function getID(name) {

        var obj = {}, d = 'dimension';
        
        if ($('#devproperty').is(':checked')) {
            switch (name) {
                //these are dev property IDs, with dimensions
                case 'advil':       obj = {'property': 'UA-55526842-2', 'd1': d+'11', 'd2': d+'13', 'd3': d+'14', 'd4': d+'16', 'd5': d+'17', 'hostname': 'www.advil.com'}; return obj;
                case 'advilpm':     obj = {'property': 'UA-55526842-4', 'd1': d+'11', 'd2': d+'13', 'd3': d+'14', 'd4': d+'16', 'd5': d+'17', 'hostname': 'pm.advil.com'}; return obj;
                case 'advilchild':  obj = {'property': 'UA-55526842-6', 'd1': d+'11', 'd2': d+'13', 'd3': d+'14', 'd4': d+'16', 'd5': d+'17', 'hostname': 'children.advil.com'}; return obj;
                case 'advilresp':   obj = {'property': 'UA-55526842-8', 'd1': d+'11', 'd2': d+'13', 'd3': d+'14', 'd4': d+'16', 'd5': d+'17', 'hostname': 'respiratory.advil.com'}; return obj;
                case 'advilaide':   obj = {'property': 'UA-55526151-14', 'd1': d+'1', 'd2': d+'3', 'd3': d+'4', 'd4': d+'5', 'd5': d+'6', 'hostname': 'advilaide.com'}; return obj;
                case 'alavert':     obj = {'property': 'UA-55546228-2', 'd1': d+'1', 'd2': d+'3', 'd3': d+'4', 'd4': d+'5', 'd5': d+'6', 'hostname': 'www.alavert.com'}; return obj;
                //case 'anbesol':	obj = {'property': 'UA-76089806-2', 'd1': d+'1', 'd2': d+'2', 'd3': d+'3', 'hostname': 'www.anbesol.com'}; return obj;
                case 'caltrate':    obj = {'property': 'UA-55524658-2', 'd1': d+'6', 'd2': d+'8', 'd3': d+'9', 'd4': d+'10', 'd5': d+'11', 'hostname': 'www.caltrate.com'}; return obj;
                case 'centrum':     obj = {'property': 'UA-55533749-2', 'd1': d+'6', 'd2': d+'8', 'd3': d+'9', 'd4': d+'10', 'd5': d+'11', 'hostname': 'www.centrum.com'}; return obj;
                case 'chapstick':   obj = {'property': 'UA-53070308-3', 'd1': d+'5', 'd2': d+'7', 'd3': d+'8', 'd4': d+'9', 'd5': d+'10', 'hostname': 'www.chapstick.com'}; return obj;
                case 'dimetapp':    obj = {'property': 'UA-55526341-2', 'd1': d+'1', 'd2': d+'3', 'd3': d+'4', 'd4': d+'5', 'd5': d+'6', 'hostname': 'www.dimetapp.com'}; return obj;
                case 'emergenc':    obj = {'property': 'UA-55526151-2', 'd1': d+'8', 'd2': d+'10', 'd3': d+'11', 'd4': d+'12', 'd5': d+'13', 'hostname': 'www.emergenc.com'}; return obj;
                case 'imedeen':     obj = {'property': 'UA-63485746-2', 'd1': d+'1', 'd2': d+'3', 'd3': d+'4', 'd4': d+'5', 'd5': d+'6', 'hostname': 'www.imedeen.com'}; return obj;
                //case 'nexium':
                //case 'nexium24':
                case 'nexium24hr':  obj = {'property':'UA-48737419-5', 'd1': d+'11', 'd2': d+'13', 'd3': d+'14', 'd4': d+'18', 'd5': d+'19', 'hostname': 'www.nexium24hr.com'}; return obj;
                case 'preparationh': obj = {'property':'UA-55524363-2', 'd1': d+'6', 'd2': d+'8', 'd3': d+'9', 'd4': d+'10', 'd5': d+'11', 'hostname': 'www.preparationh.com'}; return obj;
                case 'robitussin':  obj = {'property':'UA-52198304-4', 'd1': d+'6', 'd2': d+'8', 'd3': d+'9', 'd4': d+'10', 'd5': d+'11', 'hostname': 'www.robitussin.com'}; return obj;
                case 'thermacare':  obj = {'property':'UA-55527650-2', 'd1': d+'8', 'd2': d+'10', 'd3': d+'11', 'd4': d+'12', 'd5': d+'13', 'hostname': 'www.thermacare.com'}; return obj;
                default:            obj = {'property':'UA-59886139-1', 'd1': d+'1', 'd2': d+'2', 'd3': d+'3', 'hostname': 'www.example.com'}; return obj;
            }    
        } else if ($('#prodproperty').is(':checked')) {      
            // these are production property IDs, with dimensions
            switch (name) {
                case 'advil':       obj = {'property':'UA-55526842-1', 'd1': d+'11', 'd2': d+'13', 'd3': d+'14', 'd4': d+'16', 'd5': d+'17', 'hostname': 'www.advil.com'}; return obj;
                case 'advilpm':     obj = {'property':'UA-55526842-3', 'd1': d+'11', 'd2': d+'13', 'd3': d+'14', 'd4': d+'16', 'd5': d+'17', 'hostname': 'pm.advil.com'}; return obj;
                case 'advilchild':  obj = {'property':'UA-55526842-5', 'd1': d+'11', 'd2': d+'13', 'd3': d+'14', 'd4': d+'16', 'd5': d+'17', 'hostname': 'children.advil.com'}; return obj;
                case 'advilresp':   obj = {'property':'UA-55526842-7', 'd1': d+'11', 'd2': d+'13', 'd3': d+'14', 'd4': d+'16', 'd5': d+'17', 'hostname': 'respiratory.advil.com'}; return obj;
                case 'advilaide':   obj = {'property':'UA-55526842-13', 'd1': d+'1', 'd2': d+'3', 'd3': d+'4', 'd4': d+'5', 'd5': d+'6', 'hostname': 'advilaide.com'}; return obj;
                case 'alavert':     obj = {'property':'UA-55546228-1', 'd1': d+'1', 'd2': d+'3', 'd3': d+'4', 'd4': d+'5', 'd5': d+'6', 'hostname': 'www.alavert.com'}; return obj;
                //case 'anbesol':     obj = {'property':'UA-76089806-1', 'd1': d+'1', 'd2': d+'2', 'd3': d+'3', 'hostname': 'www.anbesol.com'}; return obj;
                case 'caltrate':    obj = {'property':'UA-55524658-1', 'd1': d+'6', 'd2': d+'8', 'd3': d+'9', 'd4': d+'10', 'd5': d+'11', 'hostname': 'www.caltrate.com'}; return obj;
                case 'centrum':     obj = {'property':'UA-55533749-1', 'd1': d+'6', 'd2': d+'8', 'd3': d+'9', 'd4': d+'10', 'd5': d+'11', 'hostname': 'www.centrum.com'}; return obj;
                case 'chapstick':   obj = {'property':'UA-53070308-2', 'd1': d+'5', 'd2': d+'7', 'd3': d+'8', 'd4': d+'9', 'd5': d+'10',  'hostname': 'www.chapstick.com'}; return obj;
                case 'dimetapp':    obj = {'property':'UA-55526341-1', 'd1': d+'1', 'd2': d+'3', 'd3': d+'4', 'd4': d+'5', 'd5': d+'6', 'hostname': 'www.dimetapp.com'}; return obj;
                case 'emergenc':    obj = {'property':'UA-55526151-1', 'd1': d+'8', 'd2': d+'10', 'd3': d+'11', 'd4': d+'12', 'd5': d+'13', 'hostname': 'www.emergenc.com'}; return obj;
                case 'imedeen':     obj = {'property':'UA-63485746-1', 'd1': d+'1', 'd2': d+'3', 'd3': d+'4', 'd4': d+'5', 'd5': d+'6', 'hostname': 'www.imedeen.com'}; return obj;
                //case 'nexium':
                //case 'nexium24':
                case 'nexium24hr':  obj = {'property':'UA-48737419-1', 'd1': d+'11', 'd2': d+'13', 'd3': d+'14', 'd4': d+'18', 'd5': d+'19', 'hostname': 'www.nexium24hr.com'}; return obj;
                case 'preparationh': obj = {'property':'UA-55524363-1', 'd1': d+'6', 'd2': d+'8', 'd3': d+'9', 'd4': d+'10', 'd5': d+'11', 'hostname': 'www.preparationh.com'}; return obj;
                case 'robitussin':  obj = {'property':'UA-52198304-1', 'd1': d+'6', 'd2': d+'8', 'd3': d+'9', 'd4': d+'10', 'd5': d+'11', 'hostname': 'www.robitussin.com'}; return obj;
                case 'thermacare':  obj = {'property':'UA-55527650-1', 'd1': d+'8', 'd2': d+'10', 'd3': d+'11', 'd4': d+'12', 'd5': d+'13', 'hostname': 'www.thermacare.com'}; return obj;
                default:            obj = {'property':'UA-59886139-1', 'd1': d+'1', 'd2': d+'2', 'd3': d+'3', 'hostname': 'www.example.com'}; return obj;
            }
        }
    }

    function generateUUID() {
        var uuid = $cid[count];
        return uuid;
    }

    function sendData() {

        var dp = '/offline-coupon-conversion';
        
        for (var i = 0; i < groupings && i < $brand.length; i++) {
            var obj = getID($brand[count]);
            var d1 = obj['d1'],
                d2 = obj['d2'],
                d3 = obj['d3'],
				d4 = obj['d4'],
				d5 = obj['d5'];
                
            ga('create', getID($brand[count]).property, 'auto', {
                "name": "t" + count,
                'clientId': generateUUID()
            });
            ga("t" + count + '.set', 'nonInteraction', true);
            ga("t" + count + '.set', 'dataSource', "revtrax");
            ga("t" + count + '.set', 'anonymizeIp', true);
			ga("t" + count + '.set', 'forceSSL', true);
            ga("t" + count + '.set', 'hostname', obj['hostname']);
            ga("t" + count + '.set', 'page', dp);
            ga("t" + count + '.set',  'location', obj['hostname'] + dp);
            ga("t" + count + '.set', d1, $cid[count]);
            ga("t" + count + '.set', d2, $date[count]);
            ga("t" + count + '.set', d3, $store[count]);
			ga("t" + count + '.set', d4, $program[count]);
			ga("t" + count + '.set', d5, $program[count]);

            if ($('#devproperty').is(':checked')) {

                // sends hit to the dev property
                ga("t" + count + '.send', 'event', $brand[count] + ' ' + random, $brand[count], $store[count], {
                    d1: $cid[count],
                    d2: $date[count],
                    d3: $store[count],
					d4: $program[count]
                });

            } else if ($('#prodproperty').is(':checked')) {

                // sends hit to the production property
                ga("t" + count + '.send', 'event', 'offline coupon conversion', 'storeName: ' + $store[count], 'brand: ' + $brand[count], {
                    d1: $cid[count],
                    d2: $date[count],
                    d3: $store[count],
					d4: $program[count]
                });

            }

            count = count + 1;
        }
        totalHits = totalHits - groupings;
        if (totalHits > 0) {
            if (totalHits < groupings) {
                groupings = totalHits;
            }

            setTimeout(function () {
                sendData();
            }, 2000); //2 seconds
            $("#status").html("You sent [" + count + "] hits out of [" + $cid.length + "] total hits.");
        } else {
            $("#status").html("You sent [" + $cid.length + "] hits in [" + (Math.abs(Date.now() - start) / 180000) + "] minutes.");

            //clear all the arrays so we can upload another file
            //to-do: make this a function and add a "Clear" button to call it
            hit = [];
            fullHit = [];
            cid = [];
            store = [];
            brand = [];
            date = [];
            $cid = [];
            $store = [];
            $brand = [];
            $date = [];
        }
    }
    sendData();
}